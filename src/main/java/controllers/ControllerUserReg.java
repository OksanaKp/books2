package controllers;

import DAO.UsersDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/userReg")
public class ControllerUserReg extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsersDAO userDAO = new UsersDAO();
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html");
        String firstName = req.getParameter("firstname");
        String lastName = req.getParameter("lastname");
        String email = req.getParameter("mail");
        String password = req.getParameter("password1");
        HttpSession session = req.getSession();

        userDAO.setUser(firstName, lastName, email, password);

        session.setAttribute("userBean", userDAO.getUser(email));
        session.setAttribute("doLogout", "Выйти");

        resp.sendRedirect("/books/user/myaccount.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        doGet(req, resp);
   }



}
