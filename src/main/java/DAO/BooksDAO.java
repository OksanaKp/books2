package DAO;

import controllers.DBConnection;
import models.Books;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


public class BooksDAO {
    private static final String fields = "SELECT books.id as bi, books.title as bookt, author, genres.title as genre, page_count" +
            " FROM books, genres where books.genre_id=genres.id";
    private static final String selectAllSQLRequest = fields+" order by books.title";

    private static final String getByGenreSQLRequest = fields+" and trim(genres.title) = ?";

    public List<Books> findAll() {
        List<Books> Books = new LinkedList<Books>();//null;
        Books book;
        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(selectAllSQLRequest);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                book = new Books(resultSet.getInt("bi"),
                        resultSet.getString("bookt"),
                        resultSet.getString("author"),
                        resultSet.getInt("page_count"),
                        resultSet.getString("genre")
                        );
                Books.add(book);
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println("find All failed...");
            e.getMessage();
        }
        return Books;
    }

    public List<Books> findAllByGenre(String genre) {
        List<Books> Books = new LinkedList<Books>();//null;
        Books book;
        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(getByGenreSQLRequest);
            preparedStatement.setString(1, genre);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                book = new Books(resultSet.getInt("bi"),
                        resultSet.getString("bookt"),
                        resultSet.getString("author"),
                        resultSet.getInt("page_count"),
                        resultSet.getString("genre")
                );
                Books.add(book);
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println("find All failed...");
            e.getMessage();
        }
        return Books;
    }

}
