package DAO;

import models.Users;
import controllers.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class UsersDAO {
    private static final String fields = "SELECT users.id, firstname, lastname, mail, passwords" +
            " FROM users ";
    private static final String selectAllSQLRequest = fields+" order by lastName";
    private static final String getByLastNameSQLRequest = fields+"where  lastName = ?";
    private static final String loginSQLRequest = fields+"where mail = ? and passwords = ?";
    private static final String mailSQLRequest = fields+"where mail = ?";
    private static final String insertAllSQLRequest = "INSERT INTO users(firstname, lastname, " +
            "mail, passwords) values(?,?,?,?)";
    private static final String editByIDSQLRequest = "UPDATE users SET firstName = ?, lastName = ?," +
            "mail = ?, passwords = ? WHERE id = ?";


    public Users findUserForLogin(String email, String password) {
        Users user = null;
        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(loginSQLRequest);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
            {
                user = new Users(resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getString("mail"),
                        resultSet.getString("passwords"),
                        resultSet.getInt("id"));
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println("login Connection failed...");
            e.getMessage();
        }
        return user;
    }

    public  int isUserEmailExists(String email) {
        int foundIndex = 0;
        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(mailSQLRequest);
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                foundIndex = resultSet.getInt("id");
            }
            connection.close();
        }
        catch (SQLException e) {
            System.out.println("Check Email Exists Connection failed...");
            e.getMessage();
        }
        return foundIndex;
    }

    public boolean setUser(String firstName, String lastName, String email,String password) {
        boolean isEmailNoDuplicated = false;
        ///проверка на уникальность введенного пользователем email
        if (isUserEmailExists(email) == 0) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(insertAllSQLRequest);
                preparedStatement.setString(1, firstName);
                preparedStatement.setString(2, lastName);
                preparedStatement.setString(3, email);
                preparedStatement.setString(4, password);
                preparedStatement.executeUpdate();
                connection.close();
            } catch (SQLException e) {
                e.getMessage();
                System.out.println("Connection failed...");
            }

            isEmailNoDuplicated = true;
        }
        System.out.println("setPerson done!");
        return isEmailNoDuplicated;
    }

    public  Users getUser(String email) {
        Users user = null;
        int id = 0;
        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(mailSQLRequest);
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new Users(resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getString("mail"),
                        resultSet.getString("passwords"),
                        resultSet.getInt("id"));
            }
            connection.close();
        }
        catch (SQLException e) {
            System.out.println("Get user Connection failed...");
            e.getMessage();
        }
        return user;
    }

    public  int getId(String email) {
        int id = 0;
        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(mailSQLRequest);
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt("id");
            }
            connection.close();
        }
        catch (SQLException e) {
            System.out.println("Check Email Exists Connection failed...");
            e.getMessage();
        }
        return id;
    }

    //редактирование уже существующих в БД данных
    public boolean editUser(int id, String firstName, String lastName, String email, String password) {
        boolean isEmailNoDuplicated = false;
        //проверка на уникальность нового email
        int newEmailIndex = isUserEmailExists(email);
        if (newEmailIndex == id || newEmailIndex == 0) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(editByIDSQLRequest);
                preparedStatement.setString(1, firstName);
                preparedStatement.setString(2, lastName);
                preparedStatement.setString(3, email);
                preparedStatement.setString(4, password);
                preparedStatement.setInt(5, id);
                preparedStatement.executeUpdate();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Connection failed...");
            }
            isEmailNoDuplicated = true;
        }
        return isEmailNoDuplicated;
    }
}
