package Filters;

import DAO.UsersDAO;
import models.Users;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

public class UserFilter implements Filter {
    private FilterConfig filterConfig;
    private static ArrayList<String> pages;  // хранилище страниц

    /**
     * Конструктор по умолчанию
     */
    public UserFilter()
    {
        // Создание хранилища страниц
        if (pages == null)
            pages = new ArrayList<String>();
    }

    /**
     * Метод инициализации фильтра
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    /**
     * Метод фильтрации
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding("UTF-8");
        servletResponse.setContentType("text/html");
        HttpSession session = ((HttpServletRequest) servletRequest).getSession();
        Users user = (Users) session.getAttribute("userBean");
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        String URI = ((HttpServletRequest)servletRequest).getRequestURI();
        if (user != null || URI.equals("http://localhost:8080/books/registration")) {
            // Разрешить request продвигаться дальше. (Перейти данный Filter)
            filterChain.doFilter(servletRequest, servletResponse);
        }
        else {
            ((HttpServletResponse) servletResponse).sendRedirect("/books/login");
        }
    }

    /**
     * Метод освобождения ресурсов
     */
    @Override
    public void destroy() {
        filterConfig = null;
    }
}
