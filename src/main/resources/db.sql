CREATE DATABASE bookexc;
CREATE TABLE users
(
    id SERIAL NOT NULL PRIMARY KEY,
    firstname CHARACTER VARYING(30),
    lastname CHARACTER VARYING(30),
    mail CHARACTER VARYING(30),
    passwords CHARACTER VARYING(30)
);

INSERT INTO users (firstName, lastName, mail, passwords)
VALUES ('Илья', 'Петров', 'petr@gmail.com','111');
INSERT INTO users (firstName, lastName, mail, passwords)
VALUES ('Иван', 'Иванов', 'ivanov@gmail.com','222');
INSERT INTO users (firstName, lastName, mail, passwords)
VALUES ('Петр', 'Петров', 'p111@gmail.com','111');

CREATE TABLE genres
(
    id SERIAL NOT NULL PRIMARY KEY,
    title CHARACTER VARYING(30)
);

INSERT INTO genres (title)  VALUES ('Учебная');
INSERT INTO genres (title)  VALUES ('Классика');

CREATE TABLE books
(
    id SERIAL NOT NULL PRIMARY KEY,
    title CHARACTER VARYING(50),
    author CHARACTER VARYING(40),
    page_count INTEGER,
    genre_id INTEGER REFERENCES genres (Id)
);

INSERT INTO books (title, author, page_count, genre_id)
VALUES ('Объектно-ориентированное программирование в С++', 'Роберт Лафоре', 1200, 1);
INSERT INTO books (title, author, page_count, genre_id)
VALUES ('Эффективная Java', 'Джошуа Блох', 900, 1);
INSERT INTO books (title, author, page_count, genre_id)
VALUES ('Сборник стихов', 'Александр Пушкин', 500, 2);
INSERT INTO books (title, author, page_count, genre_id)
VALUES ('Война и мир. Том 1', 'Лев Толстой', 1300, 2);


SELECT books.id as bi, books.title as bookt, author, genres.title as genre, page_count" +
            " FROM books, genres where books.genre_id=genres.id and trim(genres.title) = 'Классика'