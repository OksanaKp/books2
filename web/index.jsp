<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>HTML5 и CSS3 сайт</title>
  <link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet"  type="text/css"  media="all">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/popupStyle.css" media="all">
  <script src="${pageContext.request.contextPath}/js/extLibs/http_code.jquery.com_jquery-1.10.2.js"></script>
  <script src="${pageContext.request.contextPath}/js/extLibs/http_code.jquery.com_ui_1.10.4_jquery-ui.js"></script>
  <script src="${pageContext.request.contextPath}/js/extLibs/http_ajax.googleapis.com_ajax_libs_jquery_1.12.4_jquery.js"></script>
  <script src="${pageContext.request.contextPath}/js/tipOfTheDay.js"></script>
  <script src="${pageContext.request.contextPath}/js/popup.js"></script>
</head>
<body>

<%@ include file="templates/headerWithMenu.jsp" %>

  <div id="top">
    <h1>Время читать!</h1>
    <h3>Лучшие книги у нас!</h3>
  </div>

  <div id="main">
    <div class="tip">
		  <h2>Совет дня</h2>
		  <span class="tipH3" id="getTipH3"></span> 
		  <br><br>
		  <button type="button" id="tipButton" class="btn"  onclick=getRandomTip()>Другой совет</button>
		  <script>
				getTodayTip();
		   </script>
	</div>
	<span>
		<div class="imgtip">
			<img src="${pageContext.request.contextPath}/images/tip.jpg" alt=""/>
			<br>
			<a href="#" class="getModals heading" ><h3 style="text-decoration:none" 
				>Узнайте об акции!!!</h3>
			</a>
		</div>
	</span>
  </div>

  <div id="katalogs">
  </div>
  <%@ include file="templates/footer.jsp" %>
  <div class="popup-fade">
    <div class="popup">
        <a class="popup-close" href="#">Закрыть</a>
        <h3 class="tipH3">Внимание!</h3>
        <br>
        <p>Теперь вы можете приобрести книгу онлайн, зарегистрировавшись на сайте и скачав наше приложение!!!</p>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
/* <!--анимирование элементов html, body-->
   работает функция animate в течении 500мс-->
 <!--скроллим с верха страницы на расстояние, равное отступу сверху от объекта id--> */
    function slowScroll(id) {
      $('html, body').animate({
        scrollTop: $(id).offset().top
      }, 500);
    }
// <!--Функция, выполняющаяся при скролле-->
    $(document).on("scroll", function () {
      if($(window).scrollTop() === 0)
        $("header").removeClass("fixed");
      else/*если расстояние, на которое проскроллили!=0*/
        $("header").attr("class", "fixed");/*добавляем класс*/
    });
  </script>
</body>
</html>