<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>HTML5 и CSS3 сайт</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
</head>
<body> 
 <div id="contacts" class="contactPage" id="contactPage">
    <center><h5>Вход</h5></center>
    <form id="form_input" method="post" action="logincheck">
      <label for="email">Логин <span>*</span></label><br>
      <input type="text" placeholder="Введите логин" name="email" id="email"
	  pattern="[\S+@\S+\.\S+]{9,40}"><br>
      <label for="passw">Пароль <span>*</span></label><br>
	  <input type="password" placeholder="Введите пароль" name="passw" id="passw"
	  pattern="[\w]{3,30}"><br>
	  <br>
      <button type="submit" id="mess_send" class="btn" >Отправить</button>
      <br>
      <br>
	  <br><a href="registration" title="Регистрация" class=" heading">Регистрация</a>
	  <br><br><a href="main" title="На главную" class=" heading">На главную</a>
    </form>
  </div>
</body>